// Copyright 2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

var aws = require('aws-sdk');
var ses = new aws.SES({region: 'eu-west-1'});

exports.handler = (event, context, callback) => {

let subject = event['subject'];
let text = event['text'];
let toAddress = event['to'];
let sender = event['from'];
 
     var params = {
        Destination: {
            ToAddresses: [toAddress]
        },
        Message: {
            Body: {
                Text: { Data: text
                    
                }
                
            },
            
            Subject: { Data: subject
                
            }
        },
        Source: sender
    };

    
     ses.sendEmail(params, function (err, data) {
        callback(null, {err: err, data: data});
        if (err) {
            console.log(err);
            context.fail(err);
        } else {
            
            console.log(data);
            context.succeed(event);
        }
    });
};